import { defineStore } from "pinia";
import { api } from "src/boot/axios";
import { LocalStorage } from "quasar";

export const useCartStore = defineStore("cart", {
  state: () => ({
    items: [],
    orders: [],
  }),
  getters: {
    itemCount: (state) =>
      state.items.reduce((acc, item) => acc + item.quantity, 0),
    totalPrice: (state) =>
      state.items.reduce((acc, item) => acc + item.price * item.quantity, 0),
  },
  actions: {
    addItem(item) {
      this.items.push(item);
    },
    removeItem(itemId) {
      const index = this.items.findIndex((item) => item.id === itemId);

      if (index !== -1) {
        this.items.splice(index, 1);
      }
    },
    incrementItem(item) {
      item.quantity++;
    },
    decrementItem(item) {
      if (item.quantity > 1) {
        item.quantity--;
      } else {
        this.removeItem(item.id);
      }
    },
    updateQuantity({ itemId, quantity }) {
      const item = this.items.find((item) => item.id === itemId);

      if (item) {
        item.quantity = quantity;
      }
    },

    async createOrder(
      customerName,
      customerEmail,
      customerPhoneNumber,
      productIds
    ) {
      const user = LocalStorage.getItem("user");
      const headers = {
        Authorization: `Bearer ${user.access_token}`,
        "Content-Type": "application/json",
      };
      try {
        const response = await api.post(
          "/orders",
          {
            customerName: customerName,
            customerEmail: customerEmail,
            customerPhoneNumber: customerPhoneNumber,
            productIds: [...productIds],
          },
          { headers }
        );
        const { msg, order } = response.data;
        console.log(order);
        if (msg === "success") {
          return true;
        }
        return false;
      } catch (error) {
        console.error(error);
        return false;
      }
    },

    clearCart() {
      this.items = [];
    },
    async fetchOrders() {
      const user = LocalStorage.getItem("user");
      const headers = {
        Authorization: `Bearer ${user.access_token}`,
        "Content-Type": "application/json",
      };
      try {
        const response = await api.get("/orders", { headers });
        const { msg, orders } = response.data;
        console.log(orders);
        if (msg === "success") {
          this.orders = orders;
          return true;
        }
        return false;
      } catch (error) {
        console.error(error);
        return false;
      }
    },
    async editOrder(orderId, productIds) {
      const user = LocalStorage.getItem("user");
      const id = Number(orderId);
      const headers = {
        Authorization: `Bearer ${user.access_token}`,
        "Content-Type": "application/json",
      };
      try {
        const response = await api.patch(
          `/orders/${id}`,
          {
            productIds: productIds.map((id) => Number(id)),
          },
          { headers }
        );
        const { msg, order } = response.data;
        console.log(order);
        if (msg === "success") {
          return true;
        }
        return false;
      } catch (error) {
        console.error(error);
        return false;
      }
    },
    async deleteOrder(orderId) {
      const user = LocalStorage.getItem("user");
      const id = Number(orderId);
      const headers = {
        Authorization: `Bearer ${user.access_token}`,
        "Content-Type": "application/json",
      };
      try {
        const response = await api.delete(`/orders/${id}`, { headers });
        const { msg } = response.data;
        if (msg === "success") {
          return true;
        }
        return false;
      } catch (error) {
        console.error(error);
        return false;
      }
    },
  },
});
