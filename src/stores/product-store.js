import { defineStore } from "pinia";
import { api } from "src/boot/axios";
import { LocalStorage } from "quasar";

export const useProduct = defineStore("product", {
  state: () => ({
    products: [],
  }),

  getters: {},
  actions: {
    async fetchProducts() {
      const user = LocalStorage.getItem("user");
      const headers = {
        Authorization: `Bearer ${user.access_token}`,
        "Content-Type": "application/json",
      };
      try {
        const response = await api.get("/products", { headers });
        const { msg, products } = response.data;
        if (msg === "success") {
          this.products = products;
          console.log(products);
          return true;
        }
        return false;
      } catch (error) {
        console.log(error);
        return false;
      }
    },
    async addProducts(name, description, imageUrl, price) {
      const user = LocalStorage.getItem("user");
      const headers = {
        Authorization: `Bearer ${user.access_token}`,
        "Content-Type": "application/json",
      };
      try {
        const response = await api.post(
          "/products",
          {
            name,
            description,
            imageUrl,
            price,
          },
          { headers }
        );
        const { msg, products } = response.data;
        if (msg === "success") {
          console.log(products);
          return true;
        }
        return false;
      } catch (error) {
        console.log(error);
        return false;
      }
    },
    async editProducts(productId, name, description, imageUrl, price) {
      const user = LocalStorage.getItem("user");
      const id = Number(productId);
      const headers = {
        Authorization: `Bearer ${user.access_token}`,
        "Content-Type": "application/json",
      };
      try {
        const response = await api.patch(
          `/products/${id}`,
          {
            name,
            description,
            imageUrl,
            price,
          },
          { headers }
        );
        const { msg, products } = response.data;
        if (msg === "success") {
          console.log(products);
          return true;
        }
        return false;
      } catch (error) {
        console.log(error);
        return false;
      }
    },
    async deleteProducts(productId) {
      const user = LocalStorage.getItem("user");
      const id = Number(productId);
      const headers = {
        Authorization: `Bearer ${user.access_token}`,
        "Content-Type": "application/json",
      };
      try {
        const response = await api.delete(`/products/${id}`, { headers });
        const { msg } = response.data;
        if (msg === "success") {
          return true;
        }
        return false;
      } catch (error) {
        console.log(error);
        return false;
      }
    },
  },
});
