import { defineStore } from "pinia";
import { api } from "src/boot/axios";
import { LocalStorage } from "quasar";

export const useAuthStore = defineStore("auth", {
  state: () => ({
    user: LocalStorage.getItem("user") || {
      name: null,
      email: null,
      phoneNumber: null,
      role: null,
      access_token: null,
      loading: false,
      loggedIn: false,
    },
    regisrationMessage: null,
  }),

  getters: {
    isAdmin: (state) => state.user.role === "admin",
  },
  actions: {
    setUser(userData) {
      const copyOfData = { ...userData };
      LocalStorage.set("user", copyOfData);
      this.user = copyOfData;
    },
    async login(userEmail, userPassword) {
      try {
        const response = await api.post("/auth/login", {
          email: userEmail,
          password: userPassword,
        });
        const { name, email, phoneNumber, role, access_token } = response.data;
        this.setUser({
          name,
          email,
          phoneNumber,
          role,
          access_token,
          loading: false,
          loggedIn: true,
        });
        return true;
      } catch (error) {
        console.error(error);
        return false;
      }
    },
    async register(userEmail, userPassword, userPhoneNumber, userName) {
      try {
        this.regisrationMessage = null;
        const response = await api.post("/auth/register", {
          email: userEmail,
          name: userName,
          phoneNumber: userPhoneNumber,
          password: userPassword,
        });
        const { msg } = response.data.msg;

        this.regisrationMessage = msg;
        return true;
      } catch (error) {
        console.error(error);
        return false;
      }
    },
    async logout() {
      try {
        // remove user from LocalStorage
        LocalStorage.remove("user");
        // reset user state to initial value
        this.user = {
          name: null,
          email: null,
          phoneNumber: null,
          role: null,
          access_token: null,
          loading: false,
          loggedIn: false,
        };
        return true;
      } catch (error) {
        console.error(error);
        return false;
      }
    },
  },
});
