import { route } from "quasar/wrappers";
import {
  createRouter,
  createMemoryHistory,
  createWebHistory,
  createWebHashHistory,
} from "vue-router";
import routes from "./routes";
import { useAuthStore } from "src/stores/auth-store";

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function ({ store, ssrContext }) {
  const authStore = useAuthStore();
  function isAuthenticated() {
    // check if the user is authenticated
    // return true if authenticated, false otherwise
    if (authStore.user.loggedIn) {
      return true;
    }
    return false;
  }

  function isAdmin() {
    // check if the user is an admin
    // return true if admin, false otherwise
    if (authStore.user.loggedIn && authStore.isAdmin) {
      return true;
    }
    return false;
  }

  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === "history"
    ? createWebHistory
    : createWebHashHistory;

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.VUE_ROUTER_BASE),
  });
  Router.beforeEach((to, from, next) => {
    // check if the route requires authentication
    if (to.meta.requiresAuth) {
      // check if the user is authenticated
      if (!isAuthenticated()) {
        next({ path: "/" });
        return;
      }
    }

    // check if the route requires admin permission
    if (to.meta.requiresAdmin) {
      // check if the user is an admin
      if (!isAdmin()) {
        next({ path: "/" });
        return;
      }
    }

    // allow access to the route
    next();
  });
  return Router;
});
