const routes = [
  {
    path: "/signup",
    component: () => import("pages/SignupPage.vue"),
    children: [
      { path: "", component: () => import("components/SignupForm.vue") },
    ],
  },
  {
    path: "/",
    component: () => import("pages/LoginPage.vue"),
    children: [
      { path: "", component: () => import("components/LoginForm.vue") },
    ],
  },
  {
    path: "/user",
    component: () => import("layouts/MainLayout.vue"),
    children: [{ path: "", component: () => import("pages/IndexPage.vue") }],
    meta: {
      requiresAuth: true, // requires authentication to access this route
      requiresAdmin: false, // does not require admin permission
    },
  },
  {
    path: "/admin",
    component: () => import("layouts/MainLayout.vue"),
    children: [{ path: "", component: () => import("pages/AdminPage.vue") }],
    meta: {
      requiresAuth: true, // requires authentication to access this route
      requiresAdmin: true, // does not require admin permission
    },
  },

  {
    path: "/admin/manageOrders",
    component: () => import("layouts/MainLayout.vue"),
    children: [{ path: "", component: () => import("pages/ManageOrders.vue") }],
    meta: {
      requiresAuth: true, // requires authentication to access this route
      requiresAdmin: true, // does not require admin permission
    },
  },
  {
    path: "/admin/manageProducts",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/ManageProducts.vue") },
    ],
    meta: {
      requiresAuth: true, // requires authentication to access this route
      requiresAdmin: true, // does not require admin permission
    },
  },

  {
    path: "/cart",
    component: () => import("pages/cartPage.vue"),
    //children: [{ path: "", component: () => import("pages/IndexPage.vue") }],
    meta: {
      requiresAuth: true, // requires authentication to access this route
      requiresAdmin: false, // does not require admin permission
    },
  },
  {
    path: "/checkout",
    component: () => import("pages/cartPage.vue"),
    //children: [{ path: "", component: () => import("pages/IndexPage.vue") }],
    meta: {
      requiresAuth: true, // requires authentication to access this route
      requiresAdmin: false, // does not require admin permission
    },
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
